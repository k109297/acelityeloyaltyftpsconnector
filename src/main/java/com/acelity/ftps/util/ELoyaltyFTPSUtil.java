package com.acelity.ftps.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.net.ftp.FTPSClient;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.acelity.eloyalty.ftps.model.Attributes;
import com.acelity.eloyalty.ftps.model.ELoyaltyFTPSABM;
import com.acelity.eloyalty.ftps.model.Payload;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ELoyaltyFTPSUtil {

   private final static Logger logger = LoggerFactory.getLogger(ELoyaltyFTPSUtil.class);

	public String getfileContent(String ftpsserver, int ftpsport, String ftpsusername, String ftpspassword,
			String ftpscerdir, String ftpsreaddir) throws JsonGenerationException, JsonMappingException, IOException {
		logger.info("INFO-Running FTPSService Version v1.1");
		final String SERVER = ftpsserver;
		final int PORT = ftpsport;
		final String USERNAME = ftpsusername;
		final String PASSWORD = ftpspassword;
		ELoyaltyFTPSABM eLoyaltyFTPSABM = new ELoyaltyFTPSABM();
		ObjectMapper mapper = new ObjectMapper();
		try {

			Thread.sleep(10000);

			FTPSClient ftpsClient = new FTPSClient("TLS", false);
			File tlsClientAuthCertFile = new File(ftpscerdir);
			ftpsClient.setTrustManager(initTrustStore(tlsClientAuthCertFile)[0]);

			ftpsClient.connect(SERVER, PORT);
			if (ftpsClient.isConnected()) {
				logger.info("INFO-Connection successful.");
			} else {
				logger.info("INFO-Connection unsuccessful");
				return  mapper.writeValueAsString(new ELoyaltyFTPSABM());
			}

			ftpsClient.enterLocalPassiveMode();
			ftpsClient.configure(new FTPClientConfig(FTPClientConfig.SYST_NT));
			ftpsClient.setUseClientMode(true);

			ftpsClient.execPBSZ(0);
			ftpsClient.execPROT("P");
			ftpsClient.enterLocalPassiveMode();

			if (!ftpsClient.login(USERNAME, PASSWORD)) {
				logger.info("ERROR-Login unsuccessful");
				ftpsClient.logout();
				return  mapper.writeValueAsString(new ELoyaltyFTPSABM());
			} else {
				logger.info("INFO-" + ftpsClient.getStatus());
			}

			int reply = ftpsClient.getReplyCode();

			// FTPReply stores a set of constants for FTP reply codes.
			if (!FTPReply.isPositiveCompletion(reply)) {
				logger.info("ERROR-Reply code NOT Positive:" + reply);
				ftpsClient.disconnect();
				return  mapper.writeValueAsString(new ELoyaltyFTPSABM());
			}

			String path = ftpsreaddir;
			String encoded = new String(path.getBytes("UTF-8"), "ISO-8859-1");
			ftpsClient.changeWorkingDirectory(encoded);

			logger.info("INFO-Current directory is " + ftpsClient.printWorkingDirectory());
			FTPFile[] ftpFiles = ftpsClient.listFiles(path);

			if (ftpFiles != null && ftpFiles.length > 0) {
				// loop thru files
				for (FTPFile file : ftpFiles) {
					if (!file.isFile()) {
						continue;
					}

					logger.info("INFO-Processing File:" + file.getName());

					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					// get the file from the remote system
					ftpsClient.retrieveFile(file.getName(), outputStream);
					InputStream inputstream = new ByteArrayInputStream(outputStream.toByteArray());
					Reader reader = new BufferedReader(new InputStreamReader(inputstream, "UTF-8"));
					Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(reader);
					
					Attributes attributes = new Attributes();
					attributes.setPath(ftpsreaddir + "/" + file.getName());
					attributes.setSize(file.getSize());
					attributes.setName(file.getName());
					attributes.setTimestamp(new Timestamp(file.getTimestamp().getTimeInMillis()));
					eLoyaltyFTPSABM.setAttributes(attributes);
					List<Payload> payloadList = new ArrayList<Payload>();
					for (CSVRecord record : records) {
						String exportDateTime = record.get(0).replaceAll("'", "").trim();
						if ("ExportDateTime".equalsIgnoreCase(exportDateTime)) {
							continue;
						}
						String campaignName = record.get(1).replaceAll("'", "").trim();
						String dateTime = record.get(2).replaceAll("'", "").trim();
						String callResult = record.get(3).replaceAll("'", "").trim();
						String phone = record.get(4).replaceAll("'", "").trim();
						String phoneExt = record.get(5).replaceAll("'", "").trim();
						String accountNumber = record.get(6).replaceAll("'", "").trim();
						String firstName = record.get(7).replaceAll("'", "").trim();
						String lastName = record.get(8).replaceAll("'", "").trim();
						String rentalOrderNumber = record.get(9).replaceAll("'", "").trim();

						Payload payload = new Payload();
						payload.setAccountNumber(accountNumber);
						payload.setCallResult(callResult);
						payload.setCampaignName(campaignName);
						payload.setDateTime(dateTime);
						payload.setExportDateTime(exportDateTime);
						payload.setFirstName(firstName);
						payload.setLastName(lastName);
						payload.setPhone(phone);
						payload.setPhoneExt(phoneExt);
						payload.setRentalOrderNumber(rentalOrderNumber);

						payloadList.add(payload);

					}
					eLoyaltyFTPSABM.setPayload(payloadList);

					logger.info("ABM:" + eLoyaltyFTPSABM.toString());

					// close output stream
					outputStream.close();
					reader.close();
					inputstream.close();
					// Break after read one file.
					break;
				}

			} else {
				logger.info("INFO-No Files Found");
			}

			ftpsClient.logout();
			ftpsClient.disconnect();

		} catch (IOException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (InterruptedException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (CertificateException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (KeyStoreException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		}
		return  toPrettyFormat(mapper.writeValueAsString(eLoyaltyFTPSABM));

	}

	private TrustManager[] initTrustStore(File tlsClientAuthCertFile)
			throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException {
		final KeyStore trustStore = KeyStore.getInstance("JKS");
		trustStore.load(null, null);
		loadCertificates(trustStore, tlsClientAuthCertFile, CertificateFactory.getInstance("X.509"));

		logger.info("INFO-Client authentication certificate file: {}" + tlsClientAuthCertFile);

		final TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		instance.init(trustStore);
		return instance.getTrustManagers();
	}

	protected void loadCertificates(KeyStore trustStore, File certFile, CertificateFactory cf)
			throws CertificateException, KeyStoreException, IOException {
		if (certFile.isFile()) {
			final Collection<? extends Certificate> certificates = cf
					.generateCertificates(new FileInputStream(certFile));
			int i = 0;
			for (Certificate cert : certificates) {
				final String alias = certFile.getAbsolutePath() + "_" + i;
				trustStore.setCertificateEntry(alias, cert);
				i++;
				logger.info("INFO-Added certificate with alias {" + alias + "} to trust store: {" + cert + "}");
			}
		} else if (certFile.isDirectory()) {
			for (Path f : Files.newDirectoryStream(certFile.toPath())) {
				loadCertificates(trustStore, f.toFile(), cf);
			}
		}
	}
	
	public boolean moveFile(String ftpsserver, int ftpsport, String ftpsusername, String ftpspassword,String ftpscerdir, String ftpsreaddir, String sourceFile, String targetFile)
	{
		final String SERVER = ftpsserver;
		final int PORT = ftpsport;
		final String USERNAME = ftpsusername;
		final String PASSWORD = ftpspassword;
		ELoyaltyFTPSABM eLoyaltyFTPSABM = new ELoyaltyFTPSABM();
		ObjectMapper mapper = new ObjectMapper();
		try {

			Thread.sleep(10000);

			FTPSClient ftpsClient = new FTPSClient("TLS", false);
			File tlsClientAuthCertFile = new File(ftpscerdir);
			ftpsClient.setTrustManager(initTrustStore(tlsClientAuthCertFile)[0]);

			ftpsClient.connect(SERVER, PORT);
			if (ftpsClient.isConnected()) {
				logger.info("INFO-Connection successful.");
			} else {
				logger.info("INFO-Connection unsuccessful");
				return  false;
			}

			ftpsClient.enterLocalPassiveMode();
			ftpsClient.configure(new FTPClientConfig(FTPClientConfig.SYST_NT));
			ftpsClient.setUseClientMode(true);

			ftpsClient.execPBSZ(0);
			ftpsClient.execPROT("P");
			ftpsClient.enterLocalPassiveMode();

			if (!ftpsClient.login(USERNAME, PASSWORD)) {
				logger.info("ERROR-Login unsuccessful");
				ftpsClient.logout();
				return  false;
			} else {
				logger.info("INFO-" + ftpsClient.getStatus());
			}

			int reply = ftpsClient.getReplyCode();

			// FTPReply stores a set of constants for FTP reply codes.
			if (!FTPReply.isPositiveCompletion(reply)) {
				logger.info("ERROR-Reply code NOT Positive:" + reply);
				ftpsClient.disconnect();
				return  false;
			}

			String path = ftpsreaddir;
			String encoded = new String(path.getBytes("UTF-8"), "ISO-8859-1");
			ftpsClient.changeWorkingDirectory(encoded);
			ftpsClient.rename(sourceFile, targetFile);	 
			ftpsClient.logout();
			ftpsClient.disconnect();

		} catch (IOException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (InterruptedException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (CertificateException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		} catch (KeyStoreException ex) {
			ex.printStackTrace();
			logger.error("ERROR-" + ExceptionUtils.getStackTrace(ex));
		}
	 
		return true;
	}

	private java.sql.Timestamp convertDate(String idate) throws ParseException {

		// DateFormat f = new SimpleDateFormat("E MMM dd HH:mm:ss zzz yyyy")
		DateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = f.parse(idate);

		java.sql.Timestamp sqlDate = new java.sql.Timestamp(d.getTime());
		return sqlDate;
	}

	public static void main(String [] args) {
		ELoyaltyFTPSUtil eLoyaltyFTPSUtil = new ELoyaltyFTPSUtil();
		try {
			String eLoyaltyFTPSABM= eLoyaltyFTPSUtil.getfileContent("10.112.0.4", 21, "ace\\svc_acelity", "7MLa.5sX", "C:/KCI/SpringBoot/AcelityELoyaltyFTPSService/USVIAACERGR001A.crt", "/hourlyreport/read/test");
			logger.info(eLoyaltyFTPSABM);
			
			eLoyaltyFTPSUtil.moveFile("10.112.0.4", 21, "ace\\svc_acelity", "7MLa.5sX", "C:/KCI/SpringBoot/AcelityELoyaltyFTPSService/USVIAACERGR001A.crt", "/hourlyreport/read/test/","lm_Returns_Agent_Campaign_27283_20200114_1045-1100_110200.csv", "/hourlyreport/read/test/processed/lm_Returns_Agent_Campaign_27283_20200114_1045-1100_110200_ERROR.csv");
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
	
	}
	private String toPrettyFormat(String jsonString) {
		JsonParser parser = new JsonParser();

		JsonObject json = parser.parse(jsonString).getAsJsonObject();

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String prettyJson = gson.toJson(json);

		return prettyJson;
	}


}