
package com.acelity.eloyalty.ftps.model;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "symbolicLink",
    "regularFile",
    "directory",
    "path",
    "size",
    "name",
    "timestamp"
})
public class Attributes {

    @JsonProperty("symbolicLink")
    private Boolean symbolicLink;
    @JsonProperty("regularFile")
    private Boolean regularFile;
    @JsonProperty("directory")
    private Boolean directory;
    @JsonProperty("path")
    private String path;
    @JsonProperty("size")
    private long size;
    @JsonProperty("name")
    private String name;
    @JsonProperty("timestamp")
    private Timestamp timestamp;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("symbolicLink")
    public Boolean getSymbolicLink() {
        return symbolicLink;
    }

    @JsonProperty("symbolicLink")
    public void setSymbolicLink(Boolean symbolicLink) {
        this.symbolicLink = symbolicLink;
    }

    @JsonProperty("regularFile")
    public Boolean getRegularFile() {
        return regularFile;
    }

    @JsonProperty("regularFile")
    public void setRegularFile(Boolean regularFile) {
        this.regularFile = regularFile;
    }

    @JsonProperty("directory")
    public Boolean getDirectory() {
        return directory;
    }

    @JsonProperty("directory")
    public void setDirectory(Boolean directory) {
        this.directory = directory;
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    @JsonProperty("size")
    public long getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(long l) {
        this.size = l;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("timestamp")
    public Timestamp getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Timestamp timestamp2) {
        this.timestamp = timestamp2;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
