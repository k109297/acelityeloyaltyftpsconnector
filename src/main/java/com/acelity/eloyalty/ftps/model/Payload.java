
package com.acelity.eloyalty.ftps.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "'ExportDateTime'",
    "'CampaignName'",
    "'DateTime'",
    "'CallResult'",
    "'Phone'",
    "'PhoneExt'",
    "'AccountNumber'",
    "'FirstName'",
    "'LastName'",
    "'RentalOrderNumber'"
})
public class Payload {

    @JsonProperty("'ExportDateTime'")
    private String exportDateTime;
    @JsonProperty("'CampaignName'")
    private String campaignName;
    @JsonProperty("'DateTime'")
    private String dateTime;
    @JsonProperty("'CallResult'")
    private String callResult;
    @JsonProperty("'Phone'")
    private String phone;
    @JsonProperty("'PhoneExt'")
    private String phoneExt;
    @JsonProperty("'AccountNumber'")
    private String accountNumber;
    @JsonProperty("'FirstName'")
    private String firstName;
    @JsonProperty("'LastName'")
    private String lastName;
    @JsonProperty("'RentalOrderNumber'")
    private String rentalOrderNumber;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("'ExportDateTime'")
    public String getExportDateTime() {
        return exportDateTime;
    }

    @JsonProperty("'ExportDateTime'")
    public void setExportDateTime(String exportDateTime) {
        this.exportDateTime = exportDateTime;
    }

    @JsonProperty("'CampaignName'")
    public String getCampaignName() {
        return campaignName;
    }

    @JsonProperty("'CampaignName'")
    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    @JsonProperty("'DateTime'")
    public String getDateTime() {
        return dateTime;
    }

    @JsonProperty("'DateTime'")
    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @JsonProperty("'CallResult'")
    public String getCallResult() {
        return callResult;
    }

    @JsonProperty("'CallResult'")
    public void setCallResult(String callResult) {
        this.callResult = callResult;
    }

    @JsonProperty("'Phone'")
    public String getPhone() {
        return phone;
    }

    @JsonProperty("'Phone'")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @JsonProperty("'PhoneExt'")
    public String getPhoneExt() {
        return phoneExt;
    }

    @JsonProperty("'PhoneExt'")
    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    @JsonProperty("'AccountNumber'")
    public String getAccountNumber() {
        return accountNumber;
    }

    @JsonProperty("'AccountNumber'")
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @JsonProperty("'FirstName'")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("'FirstName'")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("'LastName'")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("'LastName'")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("'RentalOrderNumber'")
    public String getRentalOrderNumber() {
        return rentalOrderNumber;
    }

    @JsonProperty("'RentalOrderNumber'")
    public void setRentalOrderNumber(String rentalOrderNumber) {
        this.rentalOrderNumber = rentalOrderNumber;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
